﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace JsonWebServiceDemo {
	public class Route {
		public delegate object RouteHandler(HttpRequest r);
		public RouteHandler Handler { get; set; }
	}

	public class DemoService : IHttpHandler {
		private Dictionary<string, Route> routes;

		public DemoService() {
			routes = new Dictionary<string, Route>();

			routes.Add("Foo", new Route() {
				Handler = delegate(HttpRequest r) {
					return new { Foo = "Bar" };
				}
			});
		}

		public void ProcessRequest(HttpContext context) {
			Match pathMatch = Regex.Match(context.Request.Path, 
				@"/(?<HandlerName>\S+)/(?<RouteName>\S+)");

			context.Response.ContentType = "application/json";

			if (pathMatch.Success) {
				string routeName = pathMatch.Groups["RouteName"].Value;

				try {
					object handlerResult = routes[routeName].Handler(context.Request);
					context.Response.Output.Write(JsonConvert.SerializeObject(handlerResult));
				} catch (Exception ex) {
					context.Response.StatusCode = 400;
					context.Response.Output.Write(JsonConvert.SerializeObject(ex));
				}
			} else {
				context.Response.StatusCode = 400;
				context.Response.Output.Write(JsonConvert.SerializeObject(
					new HttpException("Missing or invalid route specified.")));
			}

			context.Response.Flush();
		}

		public bool IsReusable {
			get {
				return false;
			}
		}
	}
}